package main

import (
	"fmt"
	"io"

type MethodHTTP string

const (

POST MethodHTTP = "POST"
GET = "GET"
PUT = "PUT"
DELETE = "DELETE"
HEAD = "HEAD"
CONNECT = "CONNECT"
OPTIONS = "OPTIONS"
TRACE = "TRACE"
PATCH = "PATCH"

)

type RequestHTTP struct {
	method MethodHTTP
	url string
	path string
	queryString []KeyValue
	header []KeyValue
	body []byte
}

func NewRequest() *RequestHTTP {
	method := MethodHTTP("")
	url := "https://123456789.com"
	path :="/123456"
	queryString := []KeyValue{
	{"kv.key + "=" + kv.value}
//Método definido em keyvlaue.go
}
	header :=  []KeyValue{
	{kv.key + ":" + kv.value}
//:= Define variável "curta"
}
body := []byte("")
	
fmt.Printf("*RequestHTTP")

}
type StatusCode int
type ResponseHTTP struct {
	StatusCode StatusCode
	StatusText string
	Proto string
	Header map[string][]String
	Body io.Readcloser
	ContentLenght int64
	CLose bool
	Request *RequestHttp
//StatusCode (Exemplo: 404)
//StatusText: Leitura em texto
//Proto: Versão Http
//Header: Cabeçalho da resposta
//body.ioReadcloser: interface que agrupa método básico de leitura e fechamento ***
//COntentLenght: Tamanho da resposta em byte (64)
//CLose: Booleano que indica se vai fechar ou não após a leitura da resposta
//Request: se refere a requisição inicial
}
